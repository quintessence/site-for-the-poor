package edu.practice.se.entity;

public class Light implements Turnonable {

  @Override
  public void turnOn() {
    System.out.println(String.format("%s's on", getClass().getSimpleName()));
  }
}
