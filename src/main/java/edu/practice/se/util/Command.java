package edu.practice.se.util;

public interface Command {

  void execute();
}
