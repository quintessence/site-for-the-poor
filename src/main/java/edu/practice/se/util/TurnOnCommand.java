package edu.practice.se.util;

import edu.practice.se.entity.Turnonable;

public class TurnOnCommand implements Command {

  private final Turnonable receiver;

  public TurnOnCommand(Turnonable receiver) {
    this.receiver = receiver;
  }

  @Override
  public void execute() {
    receiver.turnOn();
  }
}
