package edu.practice.se;

import edu.practice.se.entity.Light;
import edu.practice.se.entity.Turnonable;
import edu.practice.se.util.Command;
import edu.practice.se.util.TurnOnCommand;

public class Demo {

  public static void main(String[] args) {
    Turnonable light = new Light();
    Command turnOnTheLight = new TurnOnCommand(light);

    turnOnTheLight.execute();
  }
}
