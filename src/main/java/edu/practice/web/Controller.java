package edu.practice.web;

import edu.practice.web.command.Command;
import edu.practice.web.command.CommandContainer;
import edu.practice.web.exception.ApplicationException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Actually you should never use the same request handling for #doGet and #doPost!!! I'm just too
 * lazy (or too white) to write full example of a nice program using clear Java.
 */
public class Controller extends HttpServlet {

  private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    process(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    process(request, response);
  }

  private void process(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    LOGGER.debug("Controller starts");

    String commandName = request.getParameter("command");
    LOGGER.trace("Request parameter: command --> {}", commandName);

    Command command = CommandContainer.get(commandName);
    LOGGER.trace("Obtained command --> {}", command);

    String forward = Paths.ERROR_PAGE;
    try {
      forward = command.execute(request, response);
    } catch (ApplicationException ex) {
      request.setAttribute("errorMessage", ex.getMessage());
    }
    LOGGER.trace("Forward address --> " + forward);

    LOGGER.debug("Controller finished, now go to forward address --> " + forward);

    request.getRequestDispatcher(forward).forward(request, response);
  }
}
