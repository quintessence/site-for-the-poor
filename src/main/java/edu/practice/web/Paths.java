package edu.practice.web;

public final class Paths {

  public static final String MAIN_PAGE = "/index.jsp";
  public static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
  public static final String INFO_PAGE = "/WEB-INF/jsp/info.jsp";
  public static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";

  private Paths() {
  }
}
