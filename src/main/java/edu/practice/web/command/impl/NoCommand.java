package edu.practice.web.command.impl;

import edu.practice.web.Paths;
import edu.practice.web.command.Command;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NoCommand extends Command {

  private static final Logger LOGGER = LoggerFactory.getLogger(NoCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    LOGGER.debug("Command starts");

    String errorMessage = "No such command!";
    request.setAttribute("errorMessage", errorMessage);
    LOGGER.error("Set the request attribute: errorMessage --> " + errorMessage);

    LOGGER.debug("Command finished");
    return Paths.ERROR_PAGE;
  }
}
