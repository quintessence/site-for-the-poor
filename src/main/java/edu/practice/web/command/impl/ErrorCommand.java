package edu.practice.web.command.impl;

import edu.practice.web.command.Command;
import edu.practice.web.exception.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ErrorCommand extends Command {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws ApplicationException {
    throw new ApplicationException("Something went wrong!");
  }
}
