package edu.practice.web.command.impl;

import edu.practice.web.Paths;
import edu.practice.web.command.Command;
import edu.practice.web.exception.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginCommand extends Command {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoginCommand.class);

  private static final String SECRET_WORD = "admin";

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws ApplicationException {
    if ("GET".equals(request.getMethod())) {
      return Paths.LOGIN_PAGE;
    }

    LOGGER.debug("Command starts");

    String login = request.getParameter("login");
    String password = request.getParameter("password");

    if (!SECRET_WORD.equals(login) || !SECRET_WORD.equals(password)) {
      LOGGER.warn("Attempt to log in as --> {}", login);
      throw new ApplicationException("Invalid credentials");
    }

    String username = "OLEG";
    request.setAttribute("username", username);
    LOGGER.info("Set the request attribute: username --> {}", username);

    LOGGER.debug("Command finished");
    return Paths.MAIN_PAGE;
  }
}
