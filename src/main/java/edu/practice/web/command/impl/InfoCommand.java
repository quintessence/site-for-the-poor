package edu.practice.web.command.impl;

import edu.practice.web.Paths;
import edu.practice.web.command.Command;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InfoCommand extends Command {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    return Paths.INFO_PAGE;
  }
}
