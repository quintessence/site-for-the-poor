package edu.practice.web.command;

import edu.practice.web.command.impl.ErrorCommand;
import edu.practice.web.command.impl.InfoCommand;
import edu.practice.web.command.impl.LoginCommand;
import edu.practice.web.command.impl.NoCommand;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandContainer {

  private static final Logger LOGGER = LoggerFactory.getLogger(CommandContainer.class);

  private static Map<String, Command> commands = new TreeMap<>();

  static {
    commands.put("login", new LoginCommand());
    commands.put("info", new InfoCommand());

    commands.put("error", new ErrorCommand());
    commands.put("no-command", new NoCommand());
  }

  public static Command get(String commandName) {
    if (commandName == null || !commands.containsKey(commandName)) {
      LOGGER.trace("Command not found, name --> {}", commandName);
      return commands.get("no-command");
    }

    return commands.get(commandName);
  }
}
