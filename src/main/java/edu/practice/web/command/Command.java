package edu.practice.web.command;

import edu.practice.web.exception.ApplicationException;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class Command implements Serializable {

  public abstract String execute(HttpServletRequest request, HttpServletResponse response)
      throws ApplicationException;

  @Override
  public final String toString() {
    return getClass().getSimpleName();
  }
}
