<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<h1>Welcome, <c:out value="${username}" default="anonymous, login please"/>!</h1>
</body>
</html>