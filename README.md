## About

Simple example of Front Controller (modified **Command**) **pattern** usage. The project is called "Site for the poor" assuming you're so pure that you don't even have possibility to use nice modern technologies like Spring.

## Running the application

To run this application you can simply use:
```
mvn clean package tomcat7:run
```